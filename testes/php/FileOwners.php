<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        // Array com os donos (valores do array $files)
        $owners = array_values($files);

        // Array com os arquivos (valores) agrupados por donos (chaves)
        $filesByOwners = array();

        // Percorre todo o array com os donos
        for($i = 0; $i < count($owners); $i++)
        {
            // Verifica se o dono já não foi inserido no array
            if(!array_key_exists($owners[$i], $filesByOwners))
            {
                // Adiciona o dono e seus respectivos arquivos
                $filesByOwners[$owners[$i]] = array_keys($files, $owners[$i]);
            }
        }

        // Retorna o array agrupado por donos
        return $filesByOwners;
    }
}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));