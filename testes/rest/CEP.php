<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*

Postmon é uma API para consultar CEP e encomendas de maneira fácil.

Implemente uma função que recebe CEP e retorna todos os dados reltivos ao endereço correspondente.

Exemplo:

getAddressByCep('13566400') retorna:
{
	"bairro": "Vila Marina",
	"cidade": "São Carlos",
	"logradouro": "Rua Francisco Maricondi",
	"estado_info": {
	"area_km2": "248.221,996",
	"codigo_ibge": "35",
		"nome": "São Paulo"
	},
	"cep": "13566400",
	"cidade_info": {
		"area_km2": "1136,907",
		"codigo_ibge": "3548906"
	},
	"estado": "SP"
}



Documentação:
https://postmon.com.br/


*/

class CEP
{
    public static function getAddressByCep($cep)
    {
				// URL da api com o cep para fazer a consulta
				$apiURL = "https://api.postmon.com.br/v1/cep/" . $cep;

				// Utiliza a biblioteca cURL para buscar as informações na página da api
				$ch = curl_init($apiURL);

				// Faz as configurações 
				curl_setopt_array($ch, array (
						CURLOPT_HEADER => false,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_SSL_VERIFYPEER => false,
				));

				// Executa a busca na api e armazena os dados no formato json
				$resultado = curl_exec($ch);
				curl_close($ch);

				// Retorna o json decodificado
				return json_decode($resultado, true);
    }
}


var_dump(CEP::getAddressByCep($cep));