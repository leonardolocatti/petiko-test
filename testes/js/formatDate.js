// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  // format from M/D/YYYY to YYYYMMDD

  // Separa a data onde tem '/' e retorna um array com as partes
  var apiDate = userDate.split("/");

  // Retorna uma string com as partes da data concatenada no formato adequado
  return apiDate[2] + apiDate[0] + apiDate[1];
}

console.log(formatDate("12/31/2014"));