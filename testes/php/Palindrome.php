<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/




class Palindrome
{
    public static function isPalindrome($word)
    {
        // Avança do início para o fim da palavra
        $inicio = 0;

        // Avança do fim para o início da palavra
        $fim = strlen($word) - 1;

        // Avança a palavra até os índices se encontrarem
        while($inicio <= $fim)
        {
            // Compara sem levar em conta maiúsculo e minúsculo
            if(!strcasecmp($word[$inicio], $word[$fim]) == 0)
            {
                return false;
            }
            
            // Avança os índices
            $inicio++;
            $fim--;
        }

        // Se chegou até aqui, a palavra é um palíndromo
        return true;
    }
}

echo Palindrome::isPalindrome('Deleveled');